﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using SherlockWPFExample.ViewModel;

namespace SherlockWPFExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            Window win = new MainWindow();
           // MainViewModel main = new MainViewModel();
          //  win.DataContext = main;
          //  win.Show();
        }
    }
}
